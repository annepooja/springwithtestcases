/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.restapi.data.repo.service;

import com.spring.restapi.model.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentService {

    
    private StudentRepository studentRepository;
    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> retrieveAllStudents() {
        return studentRepository.findAll();
    }

    public Optional<Student> retrieveStudent(long id) {
        Optional<Student> student = studentRepository.findById(id);
        return student;
    }

    public void deleteStudent(long id) {

        studentRepository.deleteById(id);
    }

    public Student createStudent(Student student) {

        return studentRepository.save(student);
    }

    public Student updateStudent(Student student) {

        return studentRepository.save(student);
    }
}
