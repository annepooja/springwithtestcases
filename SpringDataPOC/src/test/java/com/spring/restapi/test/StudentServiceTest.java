/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.restapi.test;

import com.spring.restapi.data.repo.service.StudentRepository;
import com.spring.restapi.data.repo.service.StudentService;
import com.spring.restapi.model.Student;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class StudentServiceTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private StudentRepository studentRepository;
    private StudentService studentService;
    @Mock
    private Student mockStudent1;
    @Mock
    private Student mockStudent2;

    @Before
    public void setUp() {
        studentService = new StudentService(studentRepository);
        mockStudent1 = new Student(1L, "Alice", "A-123");
        mockStudent2 = new Student(2L, "Bob", "B-123");
    }

    @Test
    public void studentServiceTest() {
        
        // Student createtion
        studentService.createStudent(mockStudent1);
        studentService.createStudent(mockStudent2);
        // Student retrival by id
        Optional<Student> student = studentService.retrieveStudent(mockStudent2.getId());
        assertEquals(true, student.isPresent());
        assertNotNull(student.get());
        assertEquals(mockStudent2.getName(), student.get().getName());
        //update retived student Student
        student.get().setName("Emanula");
        studentService.updateStudent(student.get());
        Optional<Student> updatedStudent = studentService.retrieveStudent(mockStudent2.getId());
        assertEquals(true, updatedStudent.isPresent());
        assertNotNull(updatedStudent.get());
        assertNotEquals(mockStudent2.getName(), updatedStudent.get().getName());
        //Fetch all Student
        List<Student> students = studentService.retrieveAllStudents();
        assertNotNull(students);
        assertEquals(4, students.size());
    }
   
}
