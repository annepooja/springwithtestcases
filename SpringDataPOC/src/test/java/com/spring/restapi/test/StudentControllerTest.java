package com.spring.restapi.test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.restapi.controller.StudentController;
import com.spring.restapi.data.repo.service.StudentService;
import com.spring.restapi.model.Student;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = StudentController.class, secure = false)
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentService studentService;
    @Mock
    private Student mockStudent1;
    @Mock
    private Student mockStudent2;
    @Mock
    private Student mockupdatedStudent;

    @Before
    public void setUp() {
        mockStudent1 = new Student(1L, "Alice", "A-123");
        mockStudent2 = new Student(2L, "Bob", "B-123");
        mockupdatedStudent = new Student(1L, "Emanula", "B-123");
    }

    @Test
    public void saveAndUpdateStudentTest() throws Exception {
        Mockito.when(studentService.createStudent(Mockito.any(Student.class))).thenReturn(mockStudent1);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/students")
                .accept(MediaType.APPLICATION_JSON).content(getObjectAsString(mockStudent1))
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
        assertEquals("http://localhost/students/1", response.getHeader(HttpHeaders.LOCATION));

        //Update case
        Mockito.when(studentService.retrieveStudent(Mockito.anyLong())).thenReturn(Optional.of(mockStudent1));
        Mockito.when(studentService.updateStudent(Mockito.any(Student.class))).thenReturn(mockupdatedStudent);
        RequestBuilder updateRequestBuilder = MockMvcRequestBuilders
                .put("/students/1")
                .accept(MediaType.APPLICATION_JSON).content(getObjectAsString(mockupdatedStudent))
                .contentType(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(updateRequestBuilder).andReturn();
        response = result.getResponse();
        assertEquals(200, response.getStatus());
        assertEquals("Student updated Successfully", response.getContentAsString());
    }

    @Test
    public void getStudentByIdTest() throws Exception {
        Mockito.when(studentService.retrieveStudent(Mockito.anyLong())).thenReturn(Optional.of(mockStudent1));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/students/1").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertNotNull(result.getResponse().getContentAsString());
        assertEquals(getObjectAsString(mockStudent1), result.getResponse().getContentAsString());
    }

    @Test
    public void listAllStudentsTest() throws Exception {

        List<Student> mockStudents = Arrays.asList(mockStudent1, mockStudent2);
        Mockito.when(studentService.retrieveAllStudents()).thenReturn(mockStudents);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/students").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertNotNull(result.getResponse().getContentAsString());
        assertEquals(mockStudents.size(), getStudentsFromJSON(result.getResponse().getContentAsString()).size());
    }

    private List<Student> getStudentsFromJSON(String json) throws Exception {
        return new ObjectMapper().readValue(json, new TypeReference<List<Student>>() {
        });
    }

    private String getObjectAsString(Object obj) throws Exception {
        return new ObjectMapper().writeValueAsString(obj);
    }
}
